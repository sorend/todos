<?php

$db = new PDO("sqlite:../../todos.db");

// init db
$db->exec("CREATE TABLE IF NOT EXISTS todos (id INTEGER PRIMARY KEY, done INTEGER DEFAULT 0, description VARCHAR(250));");

function query($sql) {
    global $db;
    $result = $db->query($sql);

    return $result->fetchAll();
}

function update($sql, $params) {
    global $db;
    $stmt = $db->prepare($sql);
    $result = $stmt->execute($params);
    return $result;
}

function redirect($path) {
    header("Location: $path");
    exit();
}

function countStats($todos) {
    $stats = array("all" => 0, "completed" => 0, "active" => 0);
    foreach ($todos as $todo) {
        if ($todo["done"])
            $stats["completed"]++;
        else
            $stats["active"]++;
        $stats["all"]++;
    }
    return $stats;
}

function filterTodos($todos, $filter) {
    if (empty($filter)) // no filter specified, return all
        return $todos;

    $filtered = array();
    foreach ($todos as $todo) {
        if ($filter == "active" && !$todo["done"])
            $filtered[] = $todo;
        else if ($filter == "completed" && $todo["done"])
            $filtered[] = $todo;
    }
    return $filtered;
}