<?php

include_once "databasehelper.php";

$description = $_POST["description"];
$filter = $_POST["filter"];

update("INSERT INTO todos (description) VALUES (?)", array($description));

redirect("todos-simple.php?filter=$filter");
