<?php

include_once "databasehelper.php";

$todos = query("SELECT id, done, description FROM todos");
$filter = isset($_GET["filter"]) ? $_GET["filter"] : "";
$filtered = filterTodos($todos, $filter);
$stats = countStats($todos);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>To Do application</title>
    <link rel="stylesheet" href="css/base.css">
</head>
<body>
<section id="todoapp">
    <header id="header">
        <h1>todos</h1>
        <form action="add-todo.php" method="POST">
            <input type="hidden" name="filter" value="<?= $filter ?>"/>
            <input id="new-todo" name="description" placeholder="What needs to be done?" autofocus>
        </form>
    </header>
    <section id="main">
        <input id="toggle-all" type="checkbox">
        <label for="toggle-all">Mark all as complete</label>
        <ul id="todo-list">
            <?php foreach ($filtered as $idx => $todo): ?>
                <li id="toDoItem_<?= $idx ?>" class="<?= $todo["done"] ? "completed" : "" ?>" ondblclick="javascript:document.getElementById('toDoItem_<?= $idx ?>').className += ' editing';document.getElementById('toDoItemName_<?= $idx ?>').focus();">
                    <div class="view">
                        <form id="toggleForm_<?= $idx ?>" action="toggle-done.php" method="POST">
                            <input type="hidden" name="id" value="<?= $todo["id"] ?>"/>
                            <input type="hidden" name="filter" value="<?= $filter ?>"/>
                            <input class="toggle" name="toggle" type="checkbox" <?= $todo["done"] ? "checked" : "" ?> onchange="javascript:document.getElementById('toggleForm_<?= $idx ?>').submit();">
                        </form>
                        <label><?= $todo["description"] ?></label>
                        <form action="delete-todo.php" method="POST">
                            <input type="hidden" name="id" value="<?= $todo["id"] ?>"/>
                            <input type="hidden" name="filter" value="<?= $filter ?>"/>
                            <button class="destroy"></button>
                        </form>
                    </div>
                    <form id="updateForm_<?= $idx ?>" action="update-todo.php" method="POST">
                        <input type="hidden" name="id" value="<?= $todo["id"] ?>"/>
                        <input type="hidden" name="filter" value="<?= $filter ?>"/>
                        <input class="edit" id="toDoItemName_<?= $idx ?>" name="description" value="<?= $todo["description"] ?>" onblur="javascript:document.getElementById('updateForm_<?= $idx ?>').submit();"/>
                    </form>
                </li>
            <?php endforeach; ?>
        </ul>
    </section>
    <footer id="footer">
        <?php if ($stats["all"] > 0): ?>
            <span id="todo-count"><strong><?= $stats["active"] ?></strong>
            <?php if ($stats["active"] == 1): ?>
              item
            <?php else: ?>
              items
            <?php endif; ?>
            left</span>
            <ul id="filters">
                <li>
                    <a <?= $filter == 'all' ? 'class="selected"' : '' ?> href="todos-simple.php">All</a>
                </li>
                <li>
                    <a <?= $filter == 'active' ? 'class="selected"' : '' ?> href="todos-simple.php?filter=active">Active</a>
                </li>
                <li>
                  <a <?= $filter == 'completed' ? 'class="selected"' : '' ?> href="todos-simple.php?filter=completed">Completed</a>
                </li>
            </ul>
            <?php if ($stats["completed"] > 0): ?>
                <form action="clear-completed.php" method="POST">
                    <input type="hidden" name="filter" value="<?= $filter ?>"/>
                    <button id="clear-completed">Clear completed (<?= $stats["completed"] ?>)</button>
                </form>
            <?php endif; ?>
        <?php endif; ?>
    </footer>
</section>
<div id="info">
    <p>Double-click to edit a todo</p>
</div>
</body>
</html>
