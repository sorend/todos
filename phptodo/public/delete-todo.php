<?php

include_once "databasehelper.php";

$id = $_POST["id"];
$filter = $_POST["filter"];

update("DELETE FROM todos WHERE id = ?", array($id));

redirect("todos-simple.php?filter=$filter");
