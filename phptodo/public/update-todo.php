<?php

include_once "databasehelper.php";

$id = $_POST["id"];
$description = $_POST["description"];
$filter = $_POST["filter"];

update("UPDATE todos SET description = ? WHERE id = ?", array($description, $id));

redirect("todos-simple.php?filter=$filter");
