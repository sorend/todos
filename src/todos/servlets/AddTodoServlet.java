package todos.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import todos.model.TodoDAO;

/**
 * Servlet to add a Todo
 */
@WebServlet("/AddTodoServlet")
public class AddTodoServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	TodoDAO dao;
	
	public AddTodoServlet() {
		dao = TodoDAO.getInstance();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		dao.addTodo(req.getParameter("description"));
		BaseTodoServlet.redirect(req, resp, "ListTodosServlet");
	}
}
