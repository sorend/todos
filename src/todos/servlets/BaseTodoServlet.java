package todos.servlets;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import todos.model.Stats;
import todos.model.Todo;

public class BaseTodoServlet extends HttpServlet {

	List<Todo> filter(List<Todo> todos, String filter) {
		if ("completed".equals(filter))
			return todos.stream().filter((x) -> x.isDone()).collect(Collectors.toList());
		else if ("active".equals(filter))
			return todos.stream().filter((x) -> !x.isDone()).collect(Collectors.toList());
		else
			return todos;
	}

	void forward(HttpServletRequest req, HttpServletResponse resp, String path) throws ServletException, IOException {
		String filter = req.getParameter("filter");
		List<Todo> filtered = filter(getTodos(), filter);
		req.setAttribute("todos", filtered);
		req.setAttribute("stats", getStats());
		req.setAttribute("filter", filter);
		req.getRequestDispatcher(path).forward(req, resp);
	}
	
	static void redirect(HttpServletRequest req, HttpServletResponse resp, String path) throws ServletException, IOException {
		String filter = req.getParameter("filter");
		if (filter != null)
			resp.sendRedirect(path + "?filter=" + filter);
		else
			resp.sendRedirect(path);
	}

	List<Todo> todos;
	Stats stats;

	public void setTodos(List<Todo> todos) {
		this.todos = todos;
		this.stats = Stats.count(this.todos);
	}
	
	public List<Todo> getTodos() {
		return todos;
	}

	public Stats getStats() {
		return stats;
	}
}
