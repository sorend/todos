package todos.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import todos.model.TodoDAO;

/**
 * Servlet to list Todos
 */
@WebServlet("/ListTodosServlet")
public class ListTodosServlet extends BaseTodoServlet {

	private static final long serialVersionUID = 1L;

	TodoDAO dao;
	
	public ListTodosServlet() {
		dao = TodoDAO.getInstance();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		setTodos(dao.findAll());
		forward(req, resp, "/todos.jsp");
	}
}
