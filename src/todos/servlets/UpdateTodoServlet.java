package todos.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import todos.model.TodoDAO;

/**
 * Servlet to update a Todo
 */
@WebServlet("/UpdateTodoServlet")
public class UpdateTodoServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	TodoDAO dao;
	
	public UpdateTodoServlet() {
		dao = TodoDAO.getInstance();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		dao.updateTodo(Integer.parseInt(req.getParameter("id")), req.getParameter("description"),
				Boolean.parseBoolean(req.getParameter("done")));
		BaseTodoServlet.redirect(req, resp, "ListTodosServlet");
	}
}
