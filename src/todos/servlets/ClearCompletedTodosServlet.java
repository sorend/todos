package todos.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import todos.model.TodoDAO;

/**
 * Servlet to delete a Todo
 */
@WebServlet("/ClearCompletedTodosServlet")
public class ClearCompletedTodosServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	TodoDAO dao;
	
	public ClearCompletedTodosServlet() {
		dao = TodoDAO.getInstance();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		dao.clearDone();
		BaseTodoServlet.redirect(req, resp, "ListTodosServlet");
	}
}
