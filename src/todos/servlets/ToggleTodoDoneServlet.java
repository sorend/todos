package todos.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import todos.model.TodoDAO;

/**
 * Servlet to add a Todo
 */
@WebServlet("/ToggleTodoDoneServlet")
public class ToggleTodoDoneServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	TodoDAO dao;
	
	public ToggleTodoDoneServlet() {
		dao = TodoDAO.getInstance();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		dao.toggleDone(Integer.parseInt(req.getParameter("id")));
		BaseTodoServlet.redirect(req, resp, "ListTodosServlet");
	}
}
