package todos.servlets;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import todos.model.Todo;
import todos.model.TodoDAO;

/**
 * Servlet for AJAX requests
 */
@WebServlet("/JsonRestServlet")
public class JsonRestServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	static String fixupString(String s) {
		return String.valueOf(s).replace("\"", "\\\"");
	}
	
	static String mapTodo(Todo todo) {
		return String.format("{\"id\":%d, \"description\":\"%s\", \"done\":%s}", 
				todo.getId(), fixupString(todo.getDescription()), String.valueOf(todo.isDone()));
	}
	
	public static void json(HttpServletRequest req, HttpServletResponse resp, List<Todo> todos) throws ServletException, IOException {
		String encoded = "[\n\t" + todos.stream().map((x) -> mapTodo(x)).collect(Collectors.joining(",\n\t")) + "\n]";
		resp.setContentType("application/json");
		resp.getWriter().write(encoded);
		resp.getWriter().flush();
	}
	
	public static void jsonOne(HttpServletRequest req, HttpServletResponse resp, Todo todo) throws ServletException, IOException {
		String encoded = mapTodo(todo);
		resp.setContentType("application/json");
		resp.getWriter().write(encoded);
		resp.getWriter().flush();
	}
	
	TodoDAO dao;
	
	public JsonRestServlet() {
		dao = TodoDAO.getInstance();
	}

	// PUT adds new todo
	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Todo newTodo = dao.addTodo(req.getParameter("description"));
		jsonOne(req, resp, newTodo);
	}

	// GET retrieves list of todos
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		json(req, resp, dao.findAll());
	}

	// POST request updates
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		dao.updateTodo(Integer.parseInt(req.getParameter("id")), req.getParameter("description"), 
				Boolean.parseBoolean(req.getParameter("done")));
	}
	
	// DELETE request deletes
	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		dao.deleteTodo(Integer.parseInt(req.getParameter("id")));
	}
}
