package todos.model;

import java.util.List;

public class Stats {
	
	public static Stats count(List<Todo> todos) {
		Stats s = new Stats();
		for (Todo todo : todos) {
			if (todo.isDone())
				s.completed++;
			else
				s.active++;
		}
		return s;
	}
	
	private int completed = 0;
	private int active = 0;
	
	public int getCompleted() {
		return completed;
	}
	
	public int getActive() {
		return active;
	}
	
	public int getAll() {
		return active + completed;
	}
}
