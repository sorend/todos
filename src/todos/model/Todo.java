package todos.model;

public class Todo implements Comparable<Todo> {
	private long id;
	private String description;
	private boolean done;
	
	void setId(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String text) {
		this.description = text;
	}
	
	public boolean isDone() {
		return done;
	}
	
	public void setDone(boolean done) {
		this.done = done;
	}
	
	@Override
	public int compareTo(Todo o) {
		return Long.compare(this.id, o.id);
	}
}
