package todos.model;

import javax.sql.DataSource;

import org.sqlite.SQLiteDataSource;

public class DatabaseHelper {

	// create datasource to a local H2 database
	public static DataSource newDataSource() {
		return newDataSource("todos");
	}
	
	public static DataSource newDataSource(String name) {
		SQLiteDataSource ds = new SQLiteDataSource();
		ds.setUrl("jdbc:sqlite:./" + name + ".db");
		return ds;
	}

}
