package todos.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

/**
 * This class implements database access for the Todos.
 */
public class TodoDAO {
	
	private static TodoDAO instance = null;
	
	public static TodoDAO getInstance() {
		synchronized (TodoDAO.class) {
			if (instance == null) {
				instance = new TodoDAO(DatabaseHelper.newDataSource());
			}
			return instance;
		}
	}

	DataSource ds;
	
	TodoDAO(DataSource ds) {
		this.ds = ds;
		initDb();
	}
	
	// Find all Todos in database
	public List<Todo> findAll() {
		return query("SELECT id, done, description FROM todos", rowMapper);
	}
	
	public Todo findOne(long id) {
		return queryOne("SELECT id, done, description FROM todos WHERE id = " + id, rowMapper);
	}
	
	// Add one Todo to the database
	public Todo addTodo(String description) {
		long key = update("INSERT INTO todos (description) VALUES (?)", description);
		return findOne(key);
	}
	
	public Todo updateTodo(int id, String description, boolean done) {
		update("UPDATE todos SET description = ?, done = ? WHERE id = ?", description, done, id);
		return findOne(id);
	}
	
	public void deleteTodo(int id) {
		update("DELETE FROM todos WHERE id = ?", id);
	}

	// Toggle done flag on a Todo
	public Todo toggleDone(int id) {
		Todo todo = queryOne("SELECT id, done, description FROM todos WHERE id = " + id, rowMapper);
		todo.setDone(!todo.isDone());
		update("UPDATE todos SET done = ? WHERE id = ?", todo.isDone(), todo.getId());
		return todo;
	}

	// Clear all done Todos from database
	public void clearDone() {
		update("DELETE FROM todos WHERE done = ?", true);
	}
	
	// non-public methods below
	
	void initDb() {
		update("CREATE TABLE IF NOT EXISTS todos (id INTEGER PRIMARY KEY, done INTEGER DEFAULT 0, description VARCHAR(250));");
	}

	// create Todo object from resultset row
	RowHandler<Todo> rowMapper = new RowHandler<Todo>() {
		@Override
		public Todo mapRow(ResultSet rs) throws SQLException {
			Todo todo = new Todo();
			todo.setDescription(rs.getString("description"));
			todo.setDone(rs.getBoolean("done"));
			todo.setId(rs.getInt("id"));
			return todo;
		}
	};
	
	RowHandler<Integer> idMapper = new RowHandler<Integer>() {
		@Override
		public Integer mapRow(ResultSet rs) throws SQLException {
			return rs.getInt(1);
		};
	};

	// update sql
	long update(String sql, Object... params) {
		try (Connection con = ds.getConnection()) {
			PreparedStatement st = con.prepareStatement(sql);
			for (int i = 0; i < params.length; i++)
				st.setObject(i + 1, params[i]);
			st.executeUpdate();
			ResultSet rs = st.getGeneratedKeys();
			if (rs.next())
				return rs.getLong(1);
			else
				return -1;
		}
		catch (SQLException e) {
			throw new RuntimeException("Error executing: " + e.getMessage(), e);
		}
	}
	
	<T> T queryOne(String sql, RowHandler<T> qh) {
		List<T> all = query(sql, qh);
		if (all.size() == 1)
			return all.get(0);
		else
			return null;
	}

	// query and map
	<T> List<T> query(String sql, RowHandler<T> qh) {
		List<T> result = new ArrayList<T>();
		try (Connection con = ds.getConnection()) {
			try (ResultSet rs = con.createStatement().executeQuery(sql)) {
				while (rs.next())
					result.add(qh.mapRow(rs));
			}
			return result;
		}
		catch (SQLException e) {
			throw new RuntimeException("Error querying: " + e.getMessage(), e);
		}
	}
	
	interface RowHandler<T> {
		T mapRow(ResultSet rs) throws SQLException;
	}
	
}
