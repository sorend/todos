
Todo list solution demos
========================

Demo of todo list using different programming techniques (JSP and AJAX).

Usage (Java)
------------

You need to have Java 8 (JDK8) installed to compile and run this application.
The demo is started using Gradle:

    # on unix
    ./gradlew jettyRun
    
    # on Windows
    gradlew.bat jettyRun
    
Once server is running, open the url in your browser [http://localhost:8080/todos/](http://localhost:8080/todos/).
    
You can run the demo with Apache Tomcat as well (open the same URL):

    # on unix
    ./gradlew tomcatRun
    
    # on Windows
    gradlew.bat tomcatRun
    
If you want to open the project in Eclipse, then use Gradle to generate the project:

    # on unix
    ./gradlew eclipse
    
    # on Windows
    gradlew.bat eclipse
    
Then chose File > Import... and import the project into your workspace.


Usage (PHP)
-----------

PHP application is in phptodo/ folder. You need to have PHP installed for this
to work. For Linux use the package manager to install, for Windows you can use
[WebPI](https://www.microsoft.com/web/downloads/platform.aspx?lang=) to install PHP. 

Install application dependencies using composer:

    php composer.phar install

Start application:

    php -S 0.0.0.0:8081 -t public/

Once application is running, open the URL in your browser:
[http://localhost:8081/todos-simple.php](http://localhost:8081/todos-simple.php).l
